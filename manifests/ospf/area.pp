define frr::ospf::area (
  Enum['absent', 'present'] $ensure = 'present',
  Variant[Boolean, Enum['message-digest']] $auth = false,
  Variant[Boolean, Enum['no-summary']] $stub = false,
  Boolean $nssa = false,
  Optional[String[1]] $access_list_export = undef,
  Optional[String[1]] $access_list_import = undef,
  Optional[String[1]] $prefix_list_export = undef,
  Optional[String[1]] $prefix_list_import = undef,
  Array[String[1]] $networks = [],
  Hash $ranges = {},
) {
  frr_ospf_area { $name:
    ensure             => $ensure,
    auth               => $auth,
    stub               => $stub,
    nssa               => $nssa,
    access_list_export => $access_list_export,
    access_list_import => $access_list_import,
    prefix_list_export => $prefix_list_export,
    prefix_list_import => $prefix_list_import,
    networks           => $networks,
  }

  $ranges.each |String[1] $range_name, Hash $range_opts| {
    frr_ospf_area_range { "${name} ${range_name}":
      ensure => $ensure,
      *      => $range_opts,
    }
  }
}
