class frr::ospf::config {
  if $frr::ospf::config_file_manage {
    file {$frr::ospf::config_file: }

    if $frr::ospf::service_manage {
      File[$frr::ospf::config_file] {
        notify => Service[$frr::ospf::service_name],
        seltype => 'etc_runtime_t'
      }
    }
  }
}
