class frr::ospf::service {
  if $frr::ospf::service_manage and $frr::ospf::service_name != $frr::zebra::service_name {
    service {$frr::ospf::service_name:
      ensure    => $frr::ospf::service_ensure,
      enable    => $frr::ospf::service_enable,
      subscribe => Package[keys($frr::packages)]
    }
  }
}
