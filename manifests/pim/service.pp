class frr::pim::service {
  if $frr::pim::service_manage and $frr::pim::service_name != $frr::zebra::service_name {
    service {$frr::pim::service_name:
      ensure    => $frr::pim::service_ensure,
      enable    => $frr::pim::service_enable,
      subscribe => Package[keys($frr::packages)]
    }
  }
}
