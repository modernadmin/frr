class frr::pim::config {
  if $frr::pim::config_file_manage {
    file {$frr::pim::config_file: }

    if $frr::pim::service_manage {
      File[$frr::pim::config_file] {
        notify => Service[$frr::pim::service_name],
        seltype => 'etc_runtime_t'
      }
    }
  }
}
