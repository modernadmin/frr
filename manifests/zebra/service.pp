class frr::zebra::service {
  if $frr::zebra::service_manage {
    service {$frr::zebra::service_name:
      ensure    => $frr::zebra::service_ensure,
      enable    => $frr::zebra::service_enable,
      subscribe => Package[keys($frr::packages)]
    }
  }
}
