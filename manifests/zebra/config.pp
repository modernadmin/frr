class frr::zebra::config {
  if $frr::zebra::config_file_manage {
    file {$frr::zebra::config_file: }

    if $frr::zebra::service_manage {
      File[$frr::zebra::config_file] {
        notify => Service[$frr::zebra::service_name],
        seltype => 'etc_runtime_t'
      }
    }
  }
}
