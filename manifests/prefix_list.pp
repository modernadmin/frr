define frr::prefix_list (
  Hash $rules = {},
) {
  $rules.reduce({}) |Hash $rules, Tuple[Integer, Hash] $rule| {
    merge($rules, { "${name} ${rule[0]}" => $rule[1] })
  }.each |String $prefix_list_name, Hash $prefix_list| {
    frr_prefix_list {$prefix_list_name:
      * => $prefix_list,
    }
  }
}
