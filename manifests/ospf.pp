class frr::ospf (
  Boolean $agentx,
  String $config_file,
  Boolean $config_file_manage,
  String $service_name,
  Boolean $service_enable,
  Enum['running', 'stopped'] $service_ensure,
  Boolean $service_manage,
  String $service_opts,
  Hash $interfaces,
  Hash $router,
  Hash $areas,
) {
  include frr::ospf::config
  include frr::ospf::service

  if $service_enable and $service_ensure == 'running' {
    $agentx_ensure = $agentx ? {
      true  => 'present',
      false => 'absent'
    }

    file_line {'ospf_agentx':
      ensure => $agentx_ensure,
      path   => $config_file,
      line   => 'agentx'
    }

    if $service_manage {
      File_line['ospf_agentx'] {
        notify => Service[$service_name]
      }
    }

    frr_ospf_router {'ospf':
      * => $router
    }

    $interfaces.each |String $interface_name, Hash $interface| {
      frr_ospf_interface {$interface_name:
        * => $interface
      }
    }

  resources { 'frr_ospf_area_range':
    purge =>  true,
  }

    $areas.each |String $area_name, Hash $area| {
      frr::ospf::area {$area_name:
        * => $area
      }
    }
  }
}
