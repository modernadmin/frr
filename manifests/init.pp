# @summary Manages common option of frr services
# @api public
# @param default_owner
#   Specifies the default owner of frr files.
# @param default_group
#   Specifies the default group of frr files.
# @param default_mode
#   Specifies the default mode of frr files.
# @param default_content
#   Specifies the default content of frr files.
# @param service_file
#   The system configuration file
# @param service_file_manage
#   Enable or disable manage the system configuration file
# @param daemons_file
#   The daemons configuration file
# @param daemons_file_manage
#   Enable or disable manage the daemons configuration file
# @param packages
#   Specifies which packages will be installed
class frr (
  String $default_owner,
  String $default_group,
  String $default_mode,
  String $default_content,
  String $service_file,
  Boolean $service_file_manage,
  String $daemons_file,
  Boolean $daemons_file_manage,
  Hash $packages,
) {
  $packages.each |String $package_name, Hash $package| {
    package {$package_name:
      * => $package
    }
  }

  File {
    owner   => $default_owner,
    group   => $default_group,
    mode    => $default_mode,
    content => $default_content,
    replace => false,
    ensure  => present,
    require => Package[keys($packages)]
  }

  contain frr::logging
  contain frr::zebra
  contain frr::bgp
  contain frr::ospf
  contain frr::pim

  if $service_file_manage {
    file {$service_file:
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      replace => true,
      content => epp('frr/frr.sysconfig.epp'),
      seltype => 'etc_runtime_t'
      
    }
  }

  if $daemons_file_manage {
    file {$daemons_file:
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      replace => true,
      content => epp('frr/frr.daemons.epp'),
      seltype => 'etc_runtime_t'
    }
  }

}
