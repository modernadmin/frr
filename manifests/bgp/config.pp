class frr::bgp::config {
  if $frr::bgp::config_file_manage {
    file {$frr::bgp::config_file: }

    if $frr::bgp::service_manage {
      File[$frr::bgp::config_file] {
        notify => Service[$frr::bgp::service_name],
        seltype => 'etc_runtime_t'
      }
    }
  }
}
