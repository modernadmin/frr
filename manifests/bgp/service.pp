class frr::bgp::service {
  if $frr::bgp::service_manage and $frr::bgp::service_name != $frr::zebra::service_name {
    service {$frr::bgp::service_name:
      ensure    => $frr::bgp::service_ensure,
      enable    => $frr::bgp::service_enable,
      subscribe => Package[keys($frr::packages)]
    }
  }
}
