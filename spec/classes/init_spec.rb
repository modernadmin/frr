require 'spec_helper'

describe 'frr' do
  let(:hiera_config) { 'spec/data/hiera.yaml' }
  let(:title) { 'frr' }
  let(:facts) {
    {
      :networking => {
        :fqdn => 'router-1.sandbox.local'
      }
    }
  }
  let(:environment) { 'production' }

  it { is_expected.to compile }
  it { is_expected.to compile.with_all_deps }

  it { is_expected.to contain_package('frr') }

  it { is_expected.to contain_service('zebra') }
  it { is_expected.to contain_service('bgpd') }
  it { is_expected.to contain_service('ospfd') }
  it { is_expected.to contain_service('pimd') }

  it do
    is_expected.to contain_file('/etc/sysconfig/frr').with_content('#
# Managed by Puppet in the production environment
#

BGPD_OPTS="-P 0"
OSPFD_OPTS="-P 0"
PIMD_OPTS="-P 0"
ZEBRA_OPTS="-P 0"
')
  end

  it { is_expected.to contain_file('/etc/frr/zebra.conf').with_owner('frr').with_group('frr').with_mode('0600') }
  it { is_expected.to contain_file('/etc/frr/bgpd.conf').with_owner('frr').with_group('frr').with_mode('0600') }
  it { is_expected.to contain_file('/etc/frr/ospfd.conf').with_owner('frr').with_group('frr').with_mode('0600') }
  it { is_expected.to contain_file('/etc/frr/pimd.conf').with_owner('frr').with_group('frr').with_mode('0600') }
end
